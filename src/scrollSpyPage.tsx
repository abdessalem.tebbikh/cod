import { Grid } from "@material-ui/core";
import React, { useRef, useState } from "react";

export interface scrollSpyItem {
  id: string;
  label: string;
  content: React.JSX.Element;
}

interface ScrollSpyMenuProps {
  items: scrollSpyItem[];
}

export const ScrollSpyPage: React.FC<ScrollSpyMenuProps> = (props) => {
  const { items } = props;

  // État pour stocker l'échelle du texte de la section active
  const [activeLabelScale, setActiveLabelScale] = useState(1);

  const [activeSection, setActiveSection] = useState(items[0].id);
  const [activeLabel, setActiveLabel] = useState(items[0].label);
  const contentRefs = useRef<Record<string, React.RefObject<HTMLDivElement>>>(
    {}
  );

  items.forEach((item) => {
    if (!contentRefs.current[item.id]) {
      contentRefs.current[item.id] = React.createRef();
    }
  });

  React.useEffect(() => {
    const newActiveLabel =
      items.find((item) => item.id === activeSection)?.label || "";
    setActiveLabel(newActiveLabel);
  }, [activeSection, items]);

  //   const handleScroll = (e: React.UIEvent<HTMLDivElement>) => {
  //     let isSectionActive = false;

  //     for (const item of items) {
  //       const ref = contentRefs.current[item.id];
  //       if (ref?.current) {
  //         const sectionTop = ref.current.offsetTop;
  //         const sectionHeight = ref.current.clientHeight;
  //         const sectionBottom = sectionTop + sectionHeight;
  //         const screenCenter = scrollPosition + (window.innerHeight / 2);

  //         if (screenCenter > sectionTop && screenCenter < sectionBottom) {
  //           if (activeSection !== item.id) {
  //             setActiveSection(item.id);
  //             setActiveScale(1.1); // Increase scale for active section
  //             isSectionActive = true;
  //           }
  //         }
  //       }
  //     }

  //     if (!isSectionActive) {
  //       setActiveScale(1); // Reset scale when no section is active
  //     }
  //   };

  const handleScroll = (e: React.UIEvent<HTMLDivElement>) => {
    const scrollPosition = e.currentTarget.scrollTop;
    const viewportHeight = e.currentTarget.offsetHeight;
    for (const item of items) {
      const ref = contentRefs.current[item.id];
      if (ref?.current) {
        const sectionTop = ref.current.offsetTop;
        const sectionHeight = ref.current.clientHeight;
        const sectionBottom = sectionTop + sectionHeight;
        const screenCenter = scrollPosition + viewportHeight / 2;

        // Calcul de la mise à l'échelle du texte pour la section active
        if (screenCenter > sectionTop && screenCenter < sectionBottom) {
          if (activeSection !== item.id) {
            setActiveSection(item.id);
          }
          const distanceToCenter = Math.abs(
            screenCenter - (sectionTop + sectionHeight / 2)
          );
          const scale = 1 + (1 - distanceToCenter / (sectionHeight / 2)) * 0.5; // Facteur d'échelle basé sur la proximité du centre
          setActiveLabelScale(scale);
          break; // Sortir de la boucle une fois la section active trouvée
        } else {
          setActiveLabelScale(1); // Remettre à l'échelle normale si non actif
        }
      }
    }
  };

  return (
    <>
      <Grid
        container
        direction="row"
        onScroll={handleScroll}
        style={{
          overflowY: "auto",
          overflowX: "hidden",
          maxHeight: "100vh",
        }}
      >
        <Grid
          item
          style={{
            position: "sticky",
            top: "50%",
            left: "50%",
            transition: "transform 0.5s ease-out",
          }}
        >
          <div style={{ transform: `scale(${activeLabelScale})` }}>
            Active Section: {activeLabel}
          </div>
        </Grid>

        <Grid item>
          {items.map((item) => (
            <div key={item.id} id={item.id} ref={contentRefs.current[item.id]}>
              {item.content}
            </div>
          ))}
        </Grid>
      </Grid>
    </>
  );
};
