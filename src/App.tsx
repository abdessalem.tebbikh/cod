import { Box } from "@material-ui/core";
import "./App.css";
import { ScrollSpyPage } from "./scrollSpyPage";



function App() {

  const exampleContent = () => (
      <Box
        sx={{
          maxWidth: '100%', 
          overflow: 'auto',
          display: 'flex',
          justifyContent: 'center', 
          alignItems: 'center', 
        }}
      >
        <img 
          src="https://digitalassets-secure.tesla.com/image/upload/f_auto,q_auto/xufyfcvqhmq36szytod7.jpg" 
          alt="Tesla Car"
          style={{ maxWidth: '100%', height: 'auto' }} // Ensures the image scales correctly
        />
      </Box>
  );
  
  return (
    <ScrollSpyPage
      items={[
        {
          id: "1",
          label: "string 1",
          content: exampleContent(),
        },  {
          id: "2",
          label: "string 2",
          content: exampleContent(),
        },  {
          id: "3",
          label: "string 3",
          content: exampleContent(),
        },  {
          id: "4",
          label: "string 4",
          content: exampleContent(),
        },  {
          id: "5",
          label: "string 5",
          content: exampleContent(),
        },  {
          id: "6",
          label: "string 6",
          content: exampleContent(),
        },
      ]}
    />
  );
}

export default App;
